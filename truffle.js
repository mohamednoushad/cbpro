// Allows us to use ES6 in our migrations and tests.
require('babel-register')

var HDWalletProvider = require("truffle-hdwallet-provider");

var mnemonic = "bean buffalo park similar ladder gown shove melody alone globe ordinary finish";

module.exports = {
  networks: {
    ganache: {
      host: '127.0.0.1',
      port: 7545,
      network_id: '*' // Match any network id
    },
    rinkeby: {
      provider : new HDWalletProvider(mnemonic,"https://rinkeby.infura.io/"),
      network_id: 4,
      gas : 4600000
    }
  }
}
