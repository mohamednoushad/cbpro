const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
  entry: ['./app/scripts/app.js','./app/assets/js/alertify.js',
  './app/assets/js/breakpoints.min.js','./app/assets/js/browser.min.js',
  './app/assets/js/jquery.min.js','./app/assets/js/leaderboard.js','./app/assets/js/lightwallet.min.js',
  './app/assets/js/main.js','./app/assets/js/poc.js','./app/assets/js/util.js'],
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: 'app.js'
  },
  plugins: [
    // Copy our app's index.html to the build folder.
    new CopyWebpackPlugin([
      { from: './app/index.html', to: "index.html" },
      { from: './app/index-2.html', to: "index-2.html" },
      { from: './app/exchange.html', to: "exchange.html" },
      { from: './app/leaderboard.html', to: "leaderboard.html" },
      { from: './app/poc_games.html', to: "poc_games.html" },
      { from: './app/shill-kit.html', to: "shill-kit.html" },
      { from: './app/url_string.html', to: "url_string.html" }
      
    ])
  ],
  node: {
  fs: 'empty'
    },
  module: {
    rules: [
      {
       test: /\.css$/,
       use: [ 'style-loader', 'css-loader' ]
     },
     {
       test: /\.html$/,
       use: ['html-loader']
     },
     {
       test: /\.(jpg|png)$/,
       use: [
         {
           loader: 'file-loader',
           options : {
             name : '[name].[ext]',
             outputPath: 'img/',
             publicPath: 'img/'
           }
         }

       ]
     }
    ],
    loaders: [
      { test: /\.(eot|woff|woff2|ttf|svg|png|jpe?g|gif)(\?\S*)?$/
        , loader: 'url?limit=100000&name=[name].[ext]'
        },
      { test: /(\.js$)|(\.jsx$)/, exclude: /node_modules/, loader: 'babel-loader' },
            { test: /(\.jade$)/, exclude: /node_modules/, loader: 'jade-loader' },
            { test: /(\.css$)/, exclude: /node_modules/, loaders: ['style-loader', 'css-loader', 'postcss-loader'] },
            { test: /(\.styl$)/, exclude: /node_modules/, loaders: ['style-loader', 'css-loader', 'stylus-loader'] },
            { test: /\.(jpe|jpg|woff|woff2|eot|ttf|svg)(\?.*$|$)/, exclude: /node_modules/, loader: 'url-loader?importLoaders=1&limit=100000' },
            { test: /\.jsx$/, exclude: /node_modules/, loader: 'react-hot-loader' },
     {
        test: /\.scss$/,
        loader: 'style-loader!css-loader!sass-loader'
    },
    {
      test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
      use: [{
          loader: 'file-loader',
          options: {
              name: '[name].[ext]',
              outputPath: 'fonts/'
          }
      }]
     },
      { test: /\.json$/, use: 'json-loader' },
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel-loader',
        query: {
          presets: ['es2015'],
          plugins: ['transform-runtime']
        }
      }
    ]
  }
}
