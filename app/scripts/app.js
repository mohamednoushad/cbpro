//import "../styles/style.scss";


import "../assets/css/alertify.core.css";
import "../assets/css/alertify.default.css";
import "../assets/css/custom.css";
import "../assets/css/font-awesome.min.css";
import "../assets/css/main.css";





// Import libraries we need.
import { default as Web3 } from 'web3'
import { default as contract } from 'truffle-contract'

// Import our contract artifacts and turn them into usable abstractions.
import myContractArtifact from '../../build/contracts/cbproToken.json'



// MetaCoin is our usable abstraction, which we'll use through the code below.
const MyCoin = contract(myContractArtifact)
// console.log(MyCoin.address);

var adminAddr = "0x47A099bdcef2069aab9d54A78841531AD7d332dd";
//var adminAddr = "0x665D3dAA678cC35Dd64f80BEE90B783594c62dbC";
console.log("this is address",adminAddr);


var keccakvalue = web3.sha3(web3.toHex(adminAddr),{ encoding: 'hex' });
console.log("this is keccakvalue",keccakvalue);

window.setDividentPercentage = function() {
  let givenPercentage = parseInt($("#enterDividentFee").text());
  //console.log(parseInt(givenPercentage));
  var divFee = 1/(givenPercentage/100);
  //console.log(divFee);
  MyCoin.deployed().then(function(contractInstance){
    contractInstance.setDividendFee(divFee,{gas: 140000, from: web3.eth.accounts[0]}).then(function(dividentUpdate){
    $("#dividentUpdateStatus").html("<p>Updated</p>");
   })
 });
}


window.getRefferalLink = function() {
  //console.log("calling Staking Requirement");

  MyCoin.deployed().then(function(contractInstance){
    contractInstance.stakingRequirement({gas: 140000, from: web3.eth.accounts[0]}).then(async function(stakingRequirement){
     var requiredToken = web3.fromWei(stakingRequirement.toNumber());
     //console.log("this is requirement",requiredToken);
     contractInstance.myTokens({gas: 140000, from: web3.eth.accounts[0]}).then(function(userToken){
       var userBalance = web3.fromWei(userToken.toNumber());
       //console.log(userBalance);
       if( parseInt(userBalance) >= parseInt(requiredToken)){
         //console.log("User Eligible for Refferel Link")
         var weblink = window.location.hostname;
         //console.log(weblink);
         var useraccount = web3.eth.accounts[0];
         //console.log(useraccount);
         var refferalLink = weblink+'?masternode='+useraccount;
         //console.log(refferalLink);
         $("#displayReffrelLink").html("<p>Your referral link is  <span><a href=//"+refferalLink+">"+refferalLink+"</a></span>");
       }else{
         //console.log("user not eligible for Referral Link");
         $("#displayReffrelLink").html("<p>You are not eligible for Referral Link</p>");
       }
     })
     })
  })
}


window.buyTokens = function() {
  if (window.location.href.indexOf('?masternode=') > 0) {
    console.log("has masternode link");
    let referralAddress = location.href.split("masternode=")[1];
    console.log("this is refferal address",referralAddress);
    //var referralAddress = "0x47a099bdcef2069aab9d54a78841531ad7d332dd";
    let givenEth = $("#inputEth").val();
    //console.log(givenEth);
    MyCoin.deployed().then(function(contractInstance){
        contractInstance.buy(referralAddress,{gas: 140000, value:web3.toWei(givenEth, "ether"),from: web3.eth.accounts[0]}).then(function(result){
          //console.log("this is the result of transaction",result);
          $("#displayBuy").html("<p>The transaction is "+result+"</p>");  
        });
      })
  
 
   }else {
     console.log("does not have masternode link");
     //console.log("calling Buy Function");
     let givenEth = $("#inputEth").val();
         MyCoin.deployed().then(function(contractInstance){
             contractInstance.sendTransaction({from:web3.eth.accounts[0],to:contractInstance.address, value:web3.toWei(givenEth, "ether")}).then(function(finalResult){
             })
       })
   }


  
  }

  window.getReffereaBalance = function() {
    MyCoin.deployed().then(function(contractInstance){
      contractInstance.myDividends(true,{gas: 140000, from: web3.eth.accounts[0]}).then(function(includesReferrealBonus){
        contractInstance.myDividends(false,{gas: 140000, from: web3.eth.accounts[0]}).then(function(withoutBonus){
         //console.log("refferal check");
         //console.log(includesReferrealBonus.toNumber());
         //console.log(withoutBonus.toNumber());
         var refferalEarning = includesReferrealBonus.toNumber() - includesReferrealBonus.toNumber();
         //console.log("this is refferal earning",web3.fromWei(refferalEarning,"ether"));
        })
      })
      })
  }
// window.buy = function() {
//   console.log("calling Buy Function");
//   let userAccount = $("#buy_address").val();
//       MyCoin.deployed().then(function(contractInstance){
//         contractInstance.buy(userAccount,{gas: 140000, from: web3.eth.accounts[0]}).then(function(result){
//           console.log("this is the result of transaction",result);
//           $("#displayBuy").html("<p>The transaction is "+result+"</p>");  
//         });
//       })
//   }

  window.disableInitialStage = function() {
    //console.log("Disable Initial Stage Function");
        MyCoin.deployed().then(function(contractInstance){
       contractInstance.disableInitialStage({gas: 140000, from: web3.eth.accounts[0]}).then(function(result){
        //console.log("this is the result of transaction",result);
        $("#disableInitialStage_result").html("Disabled");  
        })
    })
  }

    window.exit = function() {
      //console.log("Exit Function");
          MyCoin.deployed().then(function(contractInstance){
            contractInstance.exit({gas: 140000, from: web3.eth.accounts[0]}).then(function(result){
            //console.log(result);
            //console.log("done");
            $("#exitStatus").html("<p>The Transaction is "+result+"</p>");  
            })      
          })
      }

      window.reinvest = function() {
        //console.log("Reinvest Function");
            MyCoin.deployed().then(function(contractInstance){
              contractInstance.reinvest({gas: 140000, from: web3.eth.accounts[0]}).then(function(result){
                //console.log(result);
                $("#reinvestStatus").html("<p>The reInvest is "+result+"</p>"); 
              });             
            })
        }

    

        window.sell = function() {
          //console.log("Sell Function");
           let tokenCount = $("#tokenCount").val();
           var totalTokens = tokenCount * 1000000000000000000;
           //console.log(totalTokens);
              MyCoin.deployed().then(function(contractInstance){
                contractInstance.sell(totalTokens,{gas: 140000, from: web3.eth.accounts[0]}).then(function(result) {
                  //console.log(result);
                $("#sellStatus").html("<p>The sell status is "+result+"</p>");  
                })
              })
          }

        window.setTokenFactor = function() {
          let tokenCount = $("#newTokenConversion").val();
          console.log("the token count for set factor is ", tokenCount);
          MyCoin.deployed().then(function(contractInstance){
            contractInstance.setTokenFactor(tokenCount,{gas: 140000, from: web3.eth.accounts[0]}).then(function(tokenFactorUpdate){
              console.log(tokenFactorUpdate);
              $("#tokenFactorStatus").html("<p>Updated</p>");  
            })
          });
        }

        window.setStaking = function() {
          //console.log("Set Staking Function");
          let tokenCount = web3.toWei($("#inputStake").val());
                MyCoin.deployed().then(function(contractInstance){
                contractInstance.setStakingRequirement(tokenCount,{gas: 140000, from: web3.eth.accounts[0]}).then(function(result){
                   $("#stakeStatus").html("<p>Updated</p>");   
                })  
              })
          }

          window.transfer = function() {
            //console.log("Transfer Function");
            let toAddress = $("#transferAddress").val();
            let value = $("#transferAmount").val();
            //console.log(toAddress);
            //console.log(value);
                MyCoin.deployed().then(function(contractInstance){
                  contractInstance.transfer(toAddress,web3.toWei(value,"ether"),{gas: 140000, from: web3.eth.accounts[0]}).then(function(result){
                    //console.log(result);
                    //console.log("this is transfer ",result);
                    $("#transferStatus").html("<p>The Transfer Tokens is "+result+"</p>");   
                  })
                })
            }

            window.withdraw = function() {
              //console.log("Withdraw Function");
                  MyCoin.deployed().then(function(contractInstance){
                    contractInstance.withdraw({gas: 140000, from: web3.eth.accounts[0]}).then(function(result){
                      //console.log(result);
                      //console.log("this is withdraw result ",result);
                      $("#withdrawStatus").html("<p>The Withdraw Status is "+result+"</p>");   
                    })
                  })
              }


              window.isAdministrator = function() {
                //console.log("Check if is an administrator Function");
                let inputAddress = $("#inputGivenAddress").val();
                var kecckaAddress = web3.sha3(inputAddress);
                //console.log("this is kecckak address",kecckaAddress);
                    MyCoin.deployed().then(function(contractInstance){
                      contractInstance.administrators(kecckaAddress,{gas: 140000, from: web3.eth.accounts[0]}).then(function(result){
                        //console.log(result);
                        //console.log("this is the Administrator check result ",result);
                        $("#administratorStatus").html("<p>The Administrator Check result is "+result+"</p>");   
                      })
                    })
                }

                window.balance = function() {
                  //console.log("Get Balanceof Account Function");
                  let balanceCheckingAddress = $("#BalanceCheckAddress").val();
                      MyCoin.deployed().then(function(contractInstance){
                        contractInstance.balanceOf(balanceCheckingAddress,{gas: 140000, from: web3.eth.accounts[0]}).then(function(result){
                          //console.log(result);
                          //console.log("this is the balance of the account address ",web3.fromWei(result).toNumber());
                          var balance = web3.fromWei(result).toNumber();
                          $("#displayBalance").html("<p>The Token Balance of Address is "+balance+"</p>");  
                    
                        })
                      })
                  }

                Number.prototype.noExponents= function(){
                  var data= String(this).split(/[eE]/);
                  if(data.length== 1) return data[0]; 
              
                  var  z= '', sign= this<0? '-':'',
                  str= data[0].replace('.', ''),
                  mag= Number(data[1])+ 1;
              
                  if(mag<0){
                      z= sign + '0.';
                      while(mag++) z += '0';
                      return z + str.replace(/^\-/,'');
                  }
                  mag -= str.length;  
                  while(mag--) z += '0';
                  return str + z;
              }

                  window.getBuyPrice = function() {
                    //console.log("Get Buy Price Function");
                      MyCoin.deployed().then(function(contractInstance){
                          contractInstance.buyPrice({gas: 140000, from: web3.eth.accounts[0]}).then(function(result){
                            //console.log(result);
                            //console.log("this is the buy price in wei ",result);
                            var valueineth = web3.fromWei(result, 'ether').toNumber();
                            //console.log(valueineth);
                            //console.log(valueineth.noExponents())
                            $("#displayBuyPrice").html("<p>The buy price in wei is "+result+" and in ether is "+valueineth.noExponents()+"</p>");   
                          })
                        })
                    }

          window.getSellPrice = function() {
            //console.log("Sell Price Function");
            MyCoin.deployed().then(function(contractInstance){
              contractInstance.sellPrice({gas: 140000, from: web3.eth.accounts[0]}).then(function(result){
                //console.log("this is sell Price ",result);
                var sell_pprice = web3.fromWei(result,'ether').toNumber();
                //console.log(sell_pprice);
                $("#displaySellPrice").html("<p>The Sell Price is "+sell_pprice.noExponents()+"</p>"); 
                  })
                })
            }

            window.calculateEthereum = function() {
              //console.log("Approximating Ethereum for Given Tokens");
              let tokenCount = $("#tokenForEstimation").val();
              var totalTokensCount = tokenCount * 1000000000000000000;
                  MyCoin.deployed().then(function(contractInstance){
                      contractInstance.calculateEthereumReceived(totalTokensCount,{gas: 140000, from: web3.eth.accounts[0]}).then(function(result){
                        //console.log("Ethereum available for given tokens is",result);
                        var inEth = web3.fromWei(result,'ether').toNumber();
                        //console.log(inEth);
                        $("#ethereumDisplayForTokens").html("<p>Ethereum available for given tokens is "+inEth+"</p>");   
                      })
                    })
              }

              window.calculateTokens = function() {
                //console.log("Approximating Tokens for Ethereum");
                let inputEther = $("#etherForEstimation").val();
                    MyCoin.deployed().then(function(contractInstance){
                        contractInstance.calculateTokensReceived(inputEther,{gas: 140000, from: web3.eth.accounts[0]}).then(function(result){
                          //console.log("Toens available for given ethers is",result);
                          $("#TokensDisplayForEthereum").html("<p>Tokens available for given ethereum is "+result+"</p>");   
                        })
                      })
                }

                 window.getDividends = function() {
                  //console.log("Getting Divident Function");
                  let Address = $("#dividends_address").val();
                      MyCoin.deployed().then(function(contractInstance){
                       contractInstance.dividendsOf(Address,{gas: 140000, from: web3.eth.accounts[0]}).then(function(result){
                        //console.log("this is the divident",result);
                        //console.log(result.toNumber());
                        var divident_pprice = web3.fromWei(result).toNumber();
                        //console.log(divident_pprice);
                        $("#dividentstatus").html("<p>The Divident is "+divident_pprice+"</p>");   
                       })
                      })
                  }

                  window.dividentsWithBonus = function() {
                    //console.log("Getting Divident Function With or Without Referral Bonus");
                    let setCondition = $("#dividentsCondition").val();
                        MyCoin.deployed().then(function(contractInstance){
                         contractInstance.myDividends(setCondition,{gas: 140000, from: web3.eth.accounts[0]}).then(function(result){
                          //console.log("this is the divident w/wo ref balance",result);
                          var divident_pprice = web3.fromWei(result.toString());
                          //console.log(divident_pprice);
                          $("#dividentswithBonusStatus").html("<p>The Divident is "+divident_pprice+"</p>");   
                         })
                       })
                    }

                    window.getMyTokenBalance = function() {
                      //console.log("Getting User Balance");
                          MyCoin.deployed().then(function(contractInstance){
                           contractInstance.myTokens({gas: 140000, from: web3.eth.accounts[0]}).then(function(result){
                            //console.log("this is the token balance",web3.fromWei(result).toNumber());
                            $("#myTokenBalanceStatus").html("<p>The Token Balance is "+web3.fromWei(result).toNumber()+"</p>");   
                            })
                         })
                      }

                      window.checkAmbassadors = function() {
                        //console.log("Checking Ambassadors");
                            MyCoin.deployed().then(function(contractInstance){
                             contractInstance.onlyAmbassadors({gas: 140000, from: web3.eth.accounts[0]}).then(function(result){
                              //console.log("the only ambassador condition is ",result);
                              $("#ambassadorStatus").html("<p>The only ambassador condition is "+result+"</p>");   
                             })
                           })
                        }

                        window.getStakingRequirement = function() {
                          //console.log("Checking Staking Requirement");
                              MyCoin.deployed().then(function(contractInstance){
                               contractInstance.stakingRequirement({gas: 140000, from: web3.eth.accounts[0]}).then(function(result){
                                //console.log("the staking requirement is ",result.toNumber());
                                $("#stakingRequirementStatus").html("<p>The Present Staking Requirement is  "+web3.fromWei(result.toNumber())+"</p>");   
                               })
                             })
                          }

                          window.getTotalEthereum = function() {
                            //console.log("Checking Total Balance");
                                MyCoin.deployed().then(function(contractInstance){
                                 contractInstance.totalEthereumBalance({gas: 140000, from: web3.eth.accounts[0]}).then(function(result){
                                  //console.log("the Ethereum Balance is ",web3.fromWei(result).toNumber());
                                  $("#totalEthBalance").html("<p>The Ethereum Balance is  "+web3.fromWei(result.toNumber())+"</p>");   
                                 })
                               })
                            }

                            window.getTotalTokenSupply = function() {
                              //console.log("Get Total Token Supply");
                                  MyCoin.deployed().then(function(contractInstance){
                                   contractInstance.totalSupply({gas: 140000, from: web3.eth.accounts[0]}).then(function(result){
                                    //console.log("the Total Token Supply is ",web3.fromWei(result).toNumber());
                                    $("#totalTokenSupply").html("<p>The Total Token Supply is  "+ web3.fromWei(result).toNumber() +"</p>");   
                                   })
                                 })
                              }

                               window.openEtherscan = function() {
                                MyCoin.deployed().then(function(contractInstance){
                                  var contractAddress = contractInstance.address;
                                  window.open("https://rinkeby.etherscan.io/address/"+contractAddress,"_blank");
                                });

                               }



$( document ).ready(function() {
  if (typeof web3 !== 'undefined') {
    console.warn("Using web3 detected from external source like Metamask")
    // Use Mist/MetaMask's provider
    window.web3 = new Web3(web3.currentProvider);
  } else {
    console.warn("No web3 detected. Falling back to http://localhost:8545. You should remove this fallback when you deploy live, as it's inherently insecure. Consider switching to Metamask for development. More info here: http://truffleframework.com/tutorials/truffle-and-metamask");
    // fallback - use your fallback strategy (local node / hosted node + in-dapp id mgmt / fail)
    // window.web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
    //window.web3 = new Web3(new Web3.providers.HttpProvider("https://rinkeby.infura.io/"));
    window.web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));

  }

  MyCoin.setProvider(web3.currentProvider);

  MyCoin.deployed().then(function(contractInstance){
  
        contractInstance.myTokens({gas: 140000, from: web3.eth.accounts[0]}).then(function(myTokenBalance){
          //console.log("this is my token balance",myTokenBalance);
          var displayTokens = web3.fromWei(myTokenBalance).toNumber();
          //console.log("tokensloaded",displayTokens);
          $("#tokensInWallet").html(displayTokens.noExponents());
          contractInstance.myDividends('false',{gas: 140000, from: web3.eth.accounts[0]}).then(function(obtainedDivident){
            //console.log(obtainedDivident);
            //console.log(obtainedDivident.toString());
            var divident_pprice = web3.fromWei(obtainedDivident.toString());
            //console.log(divident_pprice);
            $("#ethersInWallet").html(divident_pprice);
            contractInstance.totalEthereumBalance({gas: 140000, from: web3.eth.accounts[0]}).then(function(totalEthResult){
              //console.log("the Ethereum Balance is ",web3.fromWei(totalEthResult).toNumber());
              $("#ethersInContract").html(web3.fromWei(totalEthResult.toNumber()));   
              contractInstance.totalSupply({gas: 140000, from: web3.eth.accounts[0]}).then(function(totalTokenSupply){
                //console.log("the Total Token Supply is ",web3.fromWei(totalTokenSupply).toNumber());
                $("#tokensInCirculation").html(web3.fromWei(totalTokenSupply).toNumber().noExponents()); 
                contractInstance.stakingRequirement({gas: 140000, from: web3.eth.accounts[0]}).then(function(presentRequirement){
                $("#changeStakingRequired").html(web3.fromWei(presentRequirement.toNumber())); 
                })
              })
             })
          })
        });
  
   
    contractInstance.onlyAmbassadors({gas: 140000, from: web3.eth.accounts[0]}).then(function(ambassadorCondition){
      console.log(ambassadorCondition);
      if(ambassadorCondition){
        $("#adminPhaseMessege").html('Enabled');
      }else{
        $("#adminPhaseMessege").html('Disabled');
      }
    })
    contractInstance.dividendFee_({gas: 140000, from: web3.eth.accounts[0]}).then(function(presentDivident){
      //console.log("presentDivident",(1/presentDivident.toNumber())*100);
      $("#dividentFee").html((1/presentDivident.toNumber())*100);
    })
    contractInstance.referralBalance_(web3.eth.accounts[0],{gas: 140000, from: web3.eth.accounts[0]}).then(function(myRefferalBalance){
      //console.log(myRefferalBalance);
      //console.log("this is referral balance",web3.fromWei(myRefferalBalance.toNumber()));
      $("#refferalBalance").html(web3.fromWei(myRefferalBalance.toNumber()));
    })
    contractInstance.tokenFactor({gas: 140000, from: web3.eth.accounts[0]}).then(function(tokensForOneEth){
      //console.log("this is token factor",tokensForOneEth.toNumber());
      $("#presentTokensforEth").html(tokensForOneEth.toNumber());
    })
    
    // contractInstance.onTokenPurchase({}, { fromBlock: 0, toBlock: 'latest' }).get((error, eventResult) => {
    //   if (error)
    //     console.log('Error in myEvent event handler: ' + error);
    //   else
    //   console.log(eventResult);
    //   console.log(JSON.stringify(eventResult));
    //   for (let i=0; i < eventResult.length; i++){
    //     console.log(i);
    //   // let eventName = eventResult[i].event;
    //   let customerAddress = eventResult[i].args.customerAddress;
 
    //   // incomingEthereum =eventResult[i].incomingEthereum;
    //   // tokensMinted = eventResult[i].tokensMinted;
    //   // referredBy = eventResult[i].onTokenPurchase;
    //   // console.log(eventName,customerAddress,incomingEthereum,tokensMinted,referredBy);
    //   }
    //     console.log('myEvent: ' + JSON.stringify(eventResult.args));
    // });


  });

});

// Get the input box
var textInputBuy = document.getElementById('inputEth');

// Init a timeout variable to be used below
var timeout = null;

// Listen for keystroke events
textInputBuy.onkeyup = function (e) {

    // Clear the timeout if it has already been set.
    // This will prevent the previous task from executing
    // if it has been less than <MILLISECONDS>
    clearTimeout(timeout);

    //console.log(textInputBuy.value);
   
  


    // Make a new timeout set to go off in 800ms
    timeout = setTimeout(function () {
      MyCoin.deployed().then(function(contractInstance){
        contractInstance.calculateTokensReceived(1,{gas: 140000, from: web3.eth.accounts[0]}).then(function(result){
          //console.log(result);
          var unit = result.toNumber();

          
          var TokensToReceive = textInputBuy.value * unit;

                    
          //var TokensToReceive = result.toNumber();
          $("#getTokenCount").html("<p>Approximately "+TokensToReceive+" Tokens "); 
       
        })
      })
    }, 500);
};

// Get the input box
var textInputSell = document.getElementById('tokenCount');

// Init a timeout variable to be used below
var timeout = null;

// Listen for keystroke events
textInputSell.onkeyup = function (e) {

    // Clear the timeout if it has already been set.
    // This will prevent the previous task from executing
    // if it has been less than <MILLISECONDS>
    clearTimeout(timeout);

    // Make a new timeout set to go off in 800ms
    timeout = setTimeout(function () {
      //console.log("this is the input value",textInputSell.value);
      var totalTokensCount = textInputSell.value * 1000000000000000000;
      MyCoin.deployed().then(function(contractInstance){
        contractInstance.calculateEthereumReceived(totalTokensCount,{gas: 140000, from: web3.eth.accounts[0]}).then(function(result){
  
          //console.log(result);
          //console.log("Ethereum available for given tokens is",result);
          var inEth = web3.fromWei(result,'ether').toNumber();
          //console.log(inEth);
          $("#getEtherCount").html("<p>Approximately "+inEth+" Eth ");   
        })
      })

    }, 500);
};
